# iacollect

A parallel bulk downloader for archive.org.  The `ia` utility provided by archive.org is in theory capable of it, but requires authentication to do searches.  I didnt want to authenticate, so I just glued `urllib` and `json` together in the most rudimentary way to get the list of stuff to download from their advanced search form.  Once it worked, I decided to add parallelism as a learning exercise.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need the `internetarchive` module and the `joblib` module.  I use pipenv, so that comes from:

```
pip install pipenv
pipenv install
```

but you're welcome to use a system install of the modules instead.

### Installing

Clone the repo and get the module.

### Usage

Get the collection name from the URL of the collection.  Its the bit that comes after `/details/` and usually has underscores for spaces.

The default is to run single-threaded.  If you want more threads, think about how many you'll need.  These are I/O bound threads, so you probably want more threads than you have cores to spend, considering the threads spend most of their time in a blocked state waiting on packets.  Try not to beat the heck out of the Internet Archive, though, because that's mean and they don't deserve abuse.

```
usage: iacollect.py [-h] [--types TYPES [TYPES ...]] [--mediatype MEDIATYPE]
                    [--parallel PARALLEL] [--fragile]
                    collection

Download an archive.org item or collection

positional arguments:
  collection            the collection name or item identifier

optional arguments:
  -h, --help            show this help message and exit
  --types TYPES [TYPES ...]
                        file types to download
  --mediatype MEDIATYPE
                        mediatype
  --parallel PARALLEL   number of threads
  --fragile             Fail at the very slightest problem
```

### Usage examples
Downloading an entire collection:

```
pipenv run python iacollect.py collection_name
```

Downloading an entire collection with 10 parallel threads:
```
pipenv run python iacollect.py collection_name --parallel 10
```

Downloading only software from a collection.  Mediatypes are an archive.org metadata thing, not a file thing.

```
pipenv run python iacollect.py collection_name --mediatype software
```

Download all the files in a single item.  Items seem to have names just like collections, scrape that from the last bit of the URL as well.
```
pipenv run python iacollect.py item_name
```

Downloading just ZIP files

```
pipenv run python iacollect.py collection_name --types zip 
```

Downloading multiple file types
```
pipenv run python iacollect.py collection_name --types adf adz zip pdf
```

Note that the `-h` help lies about the order of these parameters because I apparently suck at `argparse`

## Running the tests

hah.


## Built With

* My bare hands.
* Some googling

## Contributing

I guess if you want, send over a PR.

## Versioning

Master is king.

## Authors

* majick@nachomountain.com

## License

This project is in the public domain.  Do whatever.  If you're in some kind of jurisdiction that doesn't have a public domain, Creative Commons CC0 applies.

## Acknowledgments

* The Internet Archive for making this easy to do.

