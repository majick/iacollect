#!/usr/bin/python

import os
import argparse
import sys
import urllib.parse
import urllib.request
import json
from internetarchive import download
from joblib import Parallel, delayed
from threading import Lock


def download_item(item,fragile):
  print(item)
  for retries in range(1,4):
    try:
      download(item, checksum=True,destdir=collection,glob_pattern=globs,retries=6,silent=True)
      return (item, True)
    except KeyboardInterrupt:
      raise KeyboardInterrupt
    except Exception:
      if fragile:
        raise
    
    else:
       break
  return (item, False)

mylock = Lock()
p = print

def print(*a, **b):
	with mylock:
		p(*a, **b)


parser = argparse.ArgumentParser(description='Download an archive.org item or collection')
parser.add_argument('collection', nargs=1, help='the collection name or item identifier')
parser.add_argument('--types', nargs='+', help='file types to download')
parser.add_argument('--query', type=str,nargs='+', help='query instead of collection')
parser.add_argument('--mediatype',nargs=1,help='mediatype')
parser.add_argument('--parallel',type=int,default=1,help='number of threads')
parser.add_argument('--fragile',action='store_true',default=False,help='Fail at the very slightest problem')
args = parser.parse_args()

globs = []

if args.types != None:
  print('File types to download: ', args.types)
  for extension in args.types:
    globs.append('*' + extension)
    globs.append('*' + extension.upper())
if args.query != None:
    for queryseg in args.query:
        query.append(queryseg)
    collection=query
else:
    collection = args.collection[0]
    query = 'collection:' + collection + ' OR identifier:' + collection
if args.mediatype != None:
  query = 'mediatype:' + args.mediatype[0] + ' AND ' + query

url = 'https://archive.org/advancedsearch.php'
params = {
  'q' : query,
  'fl[]' : 'identifier',
  'rows' : '999999',
  'page' : '1',
  'output' : 'json'
  }

url = url + '?' + urllib.parse.urlencode(params)
searchresponse = urllib.request.urlopen(url)
response = json.loads(searchresponse.read().decode("utf-8"))
itemcount = len(response['response']['docs'])

print('Found %s in collection %s' % ( itemcount, collection) )

if itemcount == 0:
  sys.exit(0)

if not os.path.exists(collection):
  os.makedirs(collection)

print('Using %s download threads.  ' % args.parallel)

result = Parallel(n_jobs=args.parallel,prefer="threads")(delayed(download_item)(items['identifier'],args.fragile) for items in response['response']['docs'])

status = dict(result)

succeeded = []
failed = []
for item, success in status.items():
  if success:
    succeeded.append(item)
  else:
    failed.append(item)
print('\n---')
print('%s succeeded without errors:' % len(succeeded) )
for item in succeeded: print(item) 
print()
if len(failed) > 0:
    print('%s had errors but may have partially succeeded:' % len(failed) )
    for item in failed: print(item) 

